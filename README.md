# misc



## Getting started

Set up sfossdk and hadk according to HADK PDF.


## Getting sources

cd ~/hadk/

repo init -u https://github.com/mer-hybris/android.git -b hybris-18.1

copy xiaomi_surya.xml to .repo/local_manifests/

repo sync -j 30

## Building Android HAL

Scripts to fix build errors and replace files with my modified versions:

misc/removeXiaomiParts.sh

misc/moveFixupMountpoints.sh

Build HAL:

hybris-patches/apply-patches.sh --mb

source build/envsetup.sh

breakfast $DEVICE

make -j30 hybris-hal droidmedia

## Building SailfishOS

In sfossdk:

misc/bionicPathFix.sh

rpm/dhd/helpers/build_packages.sh --build=hybris/parse-android-dynparts -s rpm/parse-android-dynparts.spec

rpm/dhd/helpers/build_packages.sh
