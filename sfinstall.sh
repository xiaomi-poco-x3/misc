#! /sbin/sh

rm -rf /data/.stowaways/sailfishos/
mkdir /data/.stowaways/sailfishos/

/external_sd/busybox tar --numeric-owner -xvf /external_sd/sailfishos-surya-release-4.5.0.24.tar.bz2 -C /data/.stowaways/sailfishos

dd if=/external_sd/hybris-boot.img of=/dev/block/bootdevice/by-name/boot
